package ru.tsc.babeshko.tm.api.controller;

public interface ICommandController {

    void showSystemInfo();

    void showHelp();

    void showCommands();

    void showArguments();

    void showVersion();

    void showAbout();

    void exit();

}
